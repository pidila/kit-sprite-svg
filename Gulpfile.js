'use strict';

var gulp        = require('gulp');
var svgSprite   = require('gulp-svg-sprite');
var plumber     = require('gulp-plumber');

// configs
// ----------------------------------------------------------------------------

var iconConfig = {
  shape                 : {
    dimension           : {
      maxWidth          : 32,
      maxHeight         : 32
    },
    transform           : ['svgo'],
    dest                : 'svg'
  },
  mode                  : {
    symbol              : {
      render            : {
        css             : false,
        scss            : false,
      },
      dest              : ".",
      sprite            : "svg/icon-sprite.svg",
      example           : {
        "dest"          : "icon-sprite.html"
      }
    },
  },
  svg                   : {
    xmlDeclaration      : false,
    doctypeDeclaration  : false,
    dimensionAttributes : false
  }
};

var illusConfig = {
  shape                 : {
    dimension           : {
      maxWidth          : 64,
      maxHeight         : 64
    },
    transform           : ['svgo'],
    dest                : 'svg'
  },
  mode                  : {
    symbol              : {
      render            : {
        css             : false,
        scss            : false,
      },
      dest              : ".",
      sprite            : "svg/illus-sprite.svg",
      example           : {
        "dest"          : "illus-sprite.html"
      }
    },
  },
  svg                   : {
    xmlDeclaration      : false,
    doctypeDeclaration  : false,
    dimensionAttributes : false
  }
};

// icon-sprite
gulp.task('icon-sprite', function() {
  return gulp.src('**/*.svg', {cwd: 'source/icons'})
    .pipe(plumber())
    .pipe(svgSprite(iconConfig))
    .pipe(gulp.dest('build'));
});

// illus-sprite
gulp.task('illus-sprite', function() {
  return gulp.src('**/*.svg', {cwd: 'source/illus'})
    .pipe(plumber())
    .pipe(svgSprite(illusConfig))
    .pipe(gulp.dest('build'));
});

gulp.task('default', ['icon-sprite', 'illus-sprite']);
